# Pihole DHCP configurations

### 01-pihole.conf

`01-pihole.conf` holds general PiHole configurations like cache size, log queries level, DNS servers etc.  
This file resides in `/etc/dnsmasq.d`.
<p><em><strong>
	This file is automatically created by PiHole installation procedure and should not be used. It is only here for backup purposes.
</strong></em></p>


### 02-pihole-dhcp.conf

`02-pihole-dhcp.conf` holds PiHole configurations regarding DHCP server, like IP range, lease time etc.  
This file resides in `/etc/dnsmasq.d`.
<p><em><strong>
	This file is automatically created by PiHole web interface and should not be used. It is only here for backup purposes.
</strong></em></p>


### 04-pihole-static-dhcp.conf

`04-pihole-static-dhcp.conf` holds the static IPs configured in PiHole's DHCP server.  
Copy the file to `/etc/dnsmasq.d` and restart PiHole to make it read and apply the new configurations.

```bash
$ sudo cp 04-pihole-static-dhcp.conf /etc/dnsmasq.d/
$ pihole restartdns
```
