#!/bin/bash

terminal_width=80
terminal_width=$(stty size | awk '{print $2}')
if (( terminal_width == 60 )); then
	space_to_extra_data=47
	space_to_extra_data_with_unicode=66
else
	space_to_extra_data=60
	space_to_extra_data_with_unicode=79
fi

## Colored Output Codes
colorRed="38;5;160"
colorGreen="38;5;154"
colorYellow="38;5;226"
colorBlue="38;5;75"
colorPink="38;5;165"
colorTeal="38;5;185"
colorWhite="38;5;255"
colorPurple="38;5;201"

## Unicode signs
unicode_degree_sign=$(printf '\xC2\xB0')
unicode_check_mark=$(printf '\xE2\x9C\x94')
unicode_x_sign=$(printf '\xE2\x9C\x96')


function color () {
	echo "\e[$1m$2\e[0m"
}

function sec2time () {
	local input=$1

	if [ $input -lt 60 ]; then
		echo "$input seconds"
	else
		((days=input/86400))
		((input=input%86400))
		((hours=input/3600))
		((input=input%3600))
		((mins=input/60))

		local daysPlural="s"
		local hoursPlural="s"
		local minsPlural="s"

		if [ $days -eq 1 ]; then
			daysPlural=""
		fi

		if [ $hours -eq 1 ]; then
			hoursPlural=""
		fi

		if [ $mins -eq 1 ]; then
			minsPlural=""
		fi

	    echo "$days day$daysPlural, $hours hour$hoursPlural, $mins minute$minsPlural"
	fi
}

function append_extra_details() {
	local str="$1"
	if [[ -z $2 ]]; then
		echo "$str"
	else
		if [[ "$#" -eq 3 ]]; then
			let spaces=$space_to_extra_data_with_unicode-${#1}
		else
			let spaces=$space_to_extra_data-${#1}
		fi
		while [ $spaces -gt 0 ]; do
			str="$str "
			let spaces=spaces-1
		done
		str="$str($2)"
		echo "$str"
	fi
}


data=$(top -bn1 | head -n5)

## SSH Sessions
active_ssh="$(echo $data | awk -F, '{print $2}' | awk '{print $1}')"
## Last login date
lastLogin="Last login: $(last $(whoami) | awk 'NR==2 {print $4, $5, $6, "@", $7}')"
ssh_stats="$(color $colorTeal "Active SSH sessions: $active_ssh")"
ssh_stats="$(append_extra_details "$ssh_stats" "$lastLogin")"

## System Uptime
uptime=$(color $colorRed "Uptime: $(sec2time $(cut -d "." -f 1 /proc/uptime))")

## Running Tasks
cpu1="$(cat /proc/loadavg | awk '{print $1}')"
cpu5="$(cat /proc/loadavg | awk '{print $2}')"
cpu15="$(cat /proc/loadavg | awk '{print $3}')"
running_tasks=$(echo $data | awk -F'Tasks:' '{print $2}' | awk '{print $3}')
total_tasks="$(ps -A h | wc -l)"
tasks_stats=$(color $colorRed "Task load: $cpu1 $cpu5 $cpu15")
tasks_extra_stats="Active: $running_tasks of $total_tasks tasks"
tasks_stats="$(append_extra_details "$tasks_stats" "$tasks_extra_stats")"

## CPU Data
cpu_usage=$(echo $data | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1"%"}')
cpu_temp="$(awk -v t=$(</sys/class/thermal/thermal_zone0/temp) 'BEGIN {print ((t / 1000)) }')$unicode_degree_sign"C
cpu_freq="$(($(</sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq)/1000))MHz"
cpu_volage="$(/opt/vc/bin/vcgencmd measure_volts core | awk -F= '{print $2}' | head -c 3)"v
cpu_stats=$(color $colorWhite "CPU usage: $cpu_usage")
cpu_extra_stats="$cpu_temp, $cpu_freq, $cpu_volage"
cpu_stats="$(append_extra_details "$cpu_stats" "$cpu_extra_stats")"

## RAM Usage
ram_total=$(echo $data | awk -F'Mem' '{print $2}' | awk '{print $2}')
ram_used=$(echo $data | awk -F'Mem' '{print $2}' | awk '{print $4}')
ram_percentage="$(echo "scale=1; $ram_used *100 / $ram_total" | bc )%"
ram_total=$(free -m | grep "Mem:" | awk '{print $2}')
ram_used=$(free -m | grep "Mem:" | awk '{print $3}')
ram_stats=$(color $colorWhite "RAM usage: $ram_percentage")
ram_extra_stats="Used: ${ram_used}MB of ${ram_total}MB"
ram_stats="$(append_extra_details "$ram_stats" "$ram_extra_stats")"

#gpu_temp="$(/opt/vc/bin/vcgencmd measure_temp | cut -c "6-9")$(printf '\xC2\xB0')"C

## MicroSD Usage
mSD_percentage="$(df -h / | grep "/dev/root" | awk '{print $5}')"
mSD_total="$(df -h / | grep "/dev/root" | awk '{print $2}')"
mSD_used="$(df -h / | grep "/dev/root" | awk '{print $3}')"
mSD_stats=$(color $colorWhite "mSD usage: $mSD_percentage")
mSD_extra_stats="Used: $mSD_used of $mSD_total"
mSD_stats="$(append_extra_details "$mSD_stats" "$mSD_extra_stats")"

## Local IP Address
lan_stats="$(color $colorWhite "LAN state:")"
local_ip_addr="$(hostname -I | awk '{print $1}')"
if [ "$local_ip_addr" == "127.0.0.1" ]; then
	lan_stats="$lan_stats $(color $colorRed "Off $unicode_x_sign")"
else
	lan_stats="$lan_stats $(color $colorGreen "Ethernet $unicode_check_mark")"
fi
lan_stats="$(append_extra_details "$lan_stats" "IP: $local_ip_addr" "true")"

## External IP Address
wan_stats="$(color $colorWhite "WAN state:")"
external_ip_addr=$(dig +short myip.opendns.com @resolver1.opendns.com)
if [ -z "$external_ip_addr" ]; then
	wan_stats="$wan_stats $(color $colorRed "Off $unicode_x_sign")"
else
	wan_stats="$wan_stats $(color $colorGreen "On $unicode_check_mark")"
	wan_stats="$(append_extra_details "$wan_stats" "IP: $external_ip_addr" "true")"
fi

## SSH Sessions
logged_in_users=$(echo $data | awk -F, '{print $2}' | awk '{print $1}')
logged_in_users=$(color $colorWhite "Currently logged in users: $logged_in_users")

## PiHole status
if (( $(ps aux | grep "[p]ihole" | wc -l) >= 1 )); then
	pihole="$(color $colorGreen "Online $unicode_check_mark")";
	pihole_version=$(pihole version | head -n1 | awk '{if ($4")" == $6) {print "Up to date ("$6} else {print $4,$5,$6}}');
else
	pihole="$(color $colorRed "Offline $unicode_x_sign")";
	pihole_version=""
fi
pihole="$(color $colorWhite "Pi-hole state:") $pihole"
pihole="$(append_extra_details "$pihole" "$pihole_version" "true")"

## Apache status
if (( $(ps aux | grep "[a]pache" | wc -l) >= 1 )); then
        apache="$(color $colorGreen "Online $unicode_check_mark")"
else
        apache="$(color $colorRed "Offline $unicode_x_sign")"
fi
apache="$(color $colorWhite "Apache state:") $apache"

## MySQL status
#if (( $(ps aux | grep "[m]ysqld" | wc -l) >= 1 )); then
#        mysql="$(color $colorGreen "Online $unicode_check_mark")"
#else
#        mysql="$(color $colorRed "Offline $unicode_x_sign")"
#fi
#mysql="$(color $colorWhite "MySQL state:") $mysql"

## Cacti status
#if (( $(ps aux | grep "[c]acti" | wc -l) == 1 )); then
#        cacti="$(color $colorGreen "Online $unicode_check_mark")"
#else
#        cacti="$(color $colorRed "Offline $unicode_x_sign")"
#fi
#cacti="$(color $colorWhite "Cacti state:") $cacti"




title="           ____  _       _           _      
          |  _ \(_)     | |__   ___ | | ___ 
          | |_) | |_____| '_ \ / _ \| |/ _ \ 
          |  __/| |_____| | | | (_) | |  __/
          |_|   |_|     |_| |_|\___/|_|\___|
"

title="$(color $colorPurple "$title")"
stats="$ssh_stats\n$uptime\n$tasks_stats\n$cpu_stats\n$ram_stats\n$mSD_stats\n$wan_stats\n$lan_stats\n$pihole\n$apache"
footer=$(color $colorBlue $(printf '%*s' "$terminal_width" | tr ' ' "="))
clear
echo -e "$footer\n$title\n$stats\n$footer"
