# Logitech G15

These scripts kills and re-start the g15daemon application in order to turn the keyboard lights on and off.
This is a workaround for Debian 11 machine that is unable to set the keyboard's lights using ```g15daemon -l```.

### To use these scripts:
Because killing and restarting the `g15Daemon` app must be done as a superuser -

1. Copy the scripts to desired location.
2. Make sure both are executable.
3. Add scripts to sudoers file to be able to execute them without prompt for the superuser password.
4. In KDE Settings -> [Personalization] -> Notifications -> Configure applications
	Search for "Screen Saver", click "Configure Events" and add the scripts to the 'Run Command' section like so:
	```shell
	/usr/bin/sudo <path-to-script>
	```
