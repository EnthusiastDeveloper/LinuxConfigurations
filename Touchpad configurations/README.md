# Touchpad gestures configurations

In order to allow gestures on the touchpad - follow the following instructions:


1. Install libinput-gestures library from AUR
2. Add your user to 'input' group:
    ``` shell
    sudo gpasswd -a $USER input
    ```

libinput-gestures.conf should be located in ```/home/<user>/.config```.  

## Attach several commands to a single gesture
Since libinput-gestures does not execute the commands in a shell, it does not support complex commands with shell arguments substitutions, expansions etc.
The 4 shell scripts used to circumvent that limitation and allow executing complex shell commands.  
These scripts pinning the active window to a quarter of the screen, where the position is described by the filename.  

#### To use these scripts:
1. Copy them to the desired location
2. Run ```chmod +x``` on them 
3. Edit the relevant gestures on libinput-gestures.conf to point to the correct path.

#### Notice that:
- The scripts has hardcoded values for the screen edges based on 1920x1080 resolution.
- For any other resolution - test the desired mouse ```x```, ```y``` coordinates  with  
    ```shell
    xdotool getmouselocation
    ```


## Use a GUI application for personalized configurations:
1. Install [gestures](https://aur.archlinux.org/packages/gestures/) from AUR
2. Use the app to configure your touchpad gestures.

## Manual configurations:
1. Copy the default configuration file to user's home directory:
    ``` shell
    sudo cp /etc/libinput-gestures.conf /home/$USER/.config/libinput-gestures.conf
    ```
2. Tweak the configurations to your likings, based on [libinput-gestures](https://github.com/bulletmark/libinput-gestures/blob/master/README.md) description.
3. Test the recognized gestures by:
	``` shell
	libinput-gestures-setup stop
	libinput-gestures -d
	```
