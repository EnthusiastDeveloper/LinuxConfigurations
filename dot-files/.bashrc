# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
export HISTCONTROL=erasedups:ignoreboth
# Timestamps in history
export HISTTIMEFORMAT="[%F %T] "

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=1000
export HISTFILESIZE=2000
export HISTIGNORE="cd*:..:...::exit:c::cdc:clear:reload:[bf]g:reboot:poweroff:vp[ck]:bal:brc:*.bash_history*:git s:gits"
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
shopt -s cdspell
# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# Add ~/.local/bin to path for packages installed using pip --user
export PATH="$PATH:$HOME/.local/bin"

export EDITOR=vim
#export VISUAL=subl
export PAGER=less
export SHELLCHECK_OPTS='--shell=dash'
export LC_ALL="en_US.UTF-8"
export LANG="en_US:"
#View pacdiff files in sublime text 3
export DIFFPROG=subl

# Enable history expansion with space
# E.g. typing !!<space> will replace the !! with your last command
bind Space:magic-space

# Tab completion for sudo
complete -cf sudo
# Tab completion for man
complete -cf man


# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#####        PS1 configurations        #####
if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    if [ "$USER" = "root" ]; then
        PS1='\[\e[0;31m\]\A \[\e[1;31;5;7m\]\u\[\e[0;34m\]@\h\[\e[0;33m\] \W \[\e[0;32m\]\$\[\e[m\] '
    else
        if [ -f /usr/share/git/completion/git-prompt.sh ] || [ -f /etc/bash_completion.d/git-prompt ]; then
            ## Source git-prompt script in order to activate the '$(__git_ps1" (%s)")' part of $PS1
            [ -f /usr/share/git/completion/git-prompt.sh ] && . /usr/share/git/completion/git-prompt.sh
            [ -f /etc/bash_completion.d/git-prompt ] && . /etc/bash_completion.d/git-prompt
            GIT_PS1_SHOWDIRTYSTATE=true
            GIT_PS1_SHOWSTASHSTATE=true
            unset GIT_PS1_SHOWUNTRACKEDFILES
            GIT_PS1_SHOWUPSTREAM='auto'
            PS1='${debian_chroot:+($debian_chroot)}\[\e[0;31m\]\A \[\e[0;34m\]\u@\h \[\e[0;33m\]\W \[\e[1;33m\]$(__git_ps1 "(%s)") \[\e[0;32m\]\$\[\e[m\] '
        else
            PS1='${debian_chroot:+($debian_chroot)}\[\e[0;31m\]\A \[\e[0;34m\]\u@\h \[\e[0;33m\]\W \[\e[0;32m\]\$\[\e[m\] '
        fi
    fi
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi


# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# Enable git-completion
if [ -f /usr/share/git/completion/git-completion.bash ]; then
    . /usr/share/git/completion/git-completion.bash
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

[ -f ~/.bash_aliases ] && . ~/.bash_aliases

# My own functions are written in a designated file - include it
[ -f ~/.bash_functions ] && . ~/.bash_functions

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

