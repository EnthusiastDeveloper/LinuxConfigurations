#!/bin/bash


RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="$(tput setaf 3)"
CYAN="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
BOLD="$(tput bold)"
NOCOLOR="$(tput sgr0)"

function nyan() {
    echo
    echo -en $RED'-_-_-_-_-_-_-_'
    echo -e $NOCOLOR$BOLD$WHITE',------,'$NOCOLOR
    echo -en $YELLOW$WHIT'_-_-_-_-_-_-_-'
    echo -e $NOCOLOR$BOLD$WHITE'|   /\_/\\'$NOCOLOR
    echo -en $GREEN'-_-_-_-_-_-_-'
    echo -e $NOCOLOR$BOLD$WHITE'~|__( ^ .^)'$NOCOLOR
    echo -en $CYAN'-_-_-_-_-_-_-'
    echo -e $NOCOLOR$BOLD$WHITE'""  ""'$NOCOLOR
    echo
}

mykillall ()
{
    for pid in $(ps x | grep "$1" | awk -F'[: ]' '{ if ($(NF-3) != "grep") {print $2}}')
    do
        sudo kill -9 "$pid"
    done
}

## Create a new directory and cd into it
function md() {
    mkdir -p "$@" && cd "$@" || return 1
}

## Go up num of directories
function up() {
    cd "$(printf "%0.0s../" $(seq 1 "$1"))" || return 1
}

# Show the battery status of BT mouse
function mouse_bat
{
    ## Make sure you have upower package installed
    ## If the below variables doesn't work for you, replace their values with the mouse MAC and hidpp battery paths using `upower -e`
    mouse_mac="$(upower -e | \grep mouse_dev)"
    hid_battery="$(upower -e | \grep hidpp_battery)"

    GREEN='\033[0;32m'
    RED='\033[0;31m'
    NO_COLOR='\033[0;m'

    bat_percentage=$(upower -i "$mouse_mac" | awk '{if ($1 ~ "percentage") print $2}')
    is_charging="$(upower -i "$hid_battery" | awk '{ if ($1 ~ "state") print $2}')"
    if [ "$is_charging" == "charging" ]; then
        echo -e "${GREEN}$bat_percentage & charging${NO_COLOR}"
    else
        echo -e "${RED}$bat_percentage, discharging${NO_COLOR}"
    fi
}

## Find shorthand
function f() {
    find . -name "$1" 2>&1 | grep -v 'Permission denied'
}


## Display hard links locations for supplied filename
## If no filename is supplied - scans all files in the current directory
function findLinks() {
    PURPLE='\033[0;35m'
    NC='\033[0m' # No Color

    ls -lA "$1" | while read -r file; do
        # Only process files -> names starts with '-'
        if [[ $file != -* ]]; then
            continue
        fi
        # the file's name is the the 9th column of `ls -l`
        file=$(echo "$file" | awk '{print $9}')
        if [[ ! -r "$file" ]]; then
            echo "'$file' is not accessible :("
            continue
        fi
        num_of_links=$(ls -ld "$file" | awk '{print $2}')
        # Test if the file has more than one link, otherwise - skip it
        if [[ num_of_links -lt  2 ]]; then
            continue
        fi
        echo -e "${PURPLE}Links for $file:${NC}"
        inode=$(ls -id "$file" | awk '{print $1}' | head -1l)
        device=$(df "$file" | tail -1l | awk '{print $6}')
        find "${device}" -inum "${inode}" 2>/dev/null | sed 's/^/   /'
    done
}

function largefiles() {
    dir=$(pwd)
    size="20M"
    if [ $# -ge 1 ]; then
        dir="$1"
    fi
    if [ $# -eq 2 ]; then
        size="$2"
    fi

    find "$dir" -type f -size +"$size" -exec ls -lh {} \; 2> /dev/null | awk '{ print $5 "\t(" $6,$7,$8 ")\t" $NF }' | sort -hrk 1
}

function extract() {
    if [ -f "$1" ] ; then
        local filename
        local foldername="${filename%%.*}"
        local fullpath
        local didfolderexist=false
        filename=$(basename "$1")
        fullpath=$(perl -e 'use Cwd "abs_path";print abs_path(shift)' "$1")

        if [ -d "$foldername" ]; then
            didfolderexist=true
            read -rp "$foldername already exists, do you want to overwrite it? (y/n) " -n 1
            echo
            if [[ $REPLY =~ ^[Nn]$ ]]; then
                return
            fi
        fi
        mkdir -p "$foldername" && cd "$foldername" || return 1
        case $1 in
            *.tar.bz2) tar xjf "$fullpath" ;;
            *.tar.gz) tar xzf "$fullpath" ;;
            *.tar.xz) tar Jxvf "$fullpath" ;;
            *.tar.Z) tar xzf "$fullpath" ;;
            *.tar) tar xf "$fullpath" ;;
            *.taz) tar xzf "$fullpath" ;;
            *.tb2) tar xjf "$fullpath" ;;
            *.tbz) tar xjf "$fullpath" ;;
            *.tbz2) tar xjf "$fullpath" ;;
            *.tgz) tar xzf "$fullpath" ;;
            *.txz) tar Jxvf "$fullpath" ;;
            *.zip) unzip "$fullpath" ;;
            *) echo "'$1' cannot be extracted via ${FUNCNAME[0]}" && cd .. && ! $didfolderexist && rm -r "$foldername" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

function log-trim() {
    [[ -z $1 ]] && echo -e "Usage: ${FUNCNAME[0]} <path-to-log-file>" && return
    sed -Ei.bak --quiet 's/^.*T([0-9:]+\.[0-9]{5}).*Severity": "([A-Z]+)", "Package": "([[:alpha:]_-]+).*File": "\.\.\/\.\.\/(.*)", "Line": "([0-9]+)", (.*)/\1 \2 \3 \4:\5 \6/p' "$1"
    sed -i 's/\\n/\n/g' "$1"
}

function word-count() {
    local search_term
    local path
    # Validate command-line arguments
    if [ "$#" -lt 1 ]; then
        echo "Usage: ${FUNCNAME[0]} <string-to-search> [/path/to/directory]"
        return 1
    fi

    search_term="$1"
    if [[ -n $2 ]]; then
        path="$2"
    else
        path="$(pwd)"
    fi

    \grep -rhci "$search_term" "$path" | awk '{sum += $1} END {print sum}'
}

# Get the maintainer and uploader of a given package(s).
# Example usage: maint <dir1> <dir2> .. - will show info for each argument
#                ls cosm-* | maint - will show info for each given data
#                maint - will show info for current directory
function maint() {
    local path

    function getMaintainer() {
        [[ "$2" -eq 1 ]] && echo "-- $1"
        res=""
        for file in "$1/debian/control"*; do
            output=$(awk '/^(Maintainer|Uploaders):/ {print $0; capt=1; next} capt && /^ / {print $0; next} capt && /^[^ ]/ {capt=0; exit}' "$file")
            res="$res$output\n"
        done

        echo -e "$res" | awk '!seen[$0]++ && NF'
    }

    # Read input from argument
    if [ $# -gt 0 ]; then
        with_dir=$(($# > 1))
        for arg in "$@"; do
            getMaintainer "$arg" $with_dir
        done
    # Use current directory if no input given from pipe
    elif [ -t 0 ]; then
        getMaintainer "$(pwd)"
    else
        # Iterate over pipe input and execute for each line
        while read -r path; do
            getMaintainer "$path" "1"
        done
    fi
}

############################# Git #############################

# Switch to side-branch based on partial string of its name
function git-chb() {
    [[ -z $1 ]] && echo -e "Error! argument is missing.\nUsage: ${FUNCNAME[0]} <partial string from target branch>" && return 1
    local br
    br="$(git branch --color=never | \grep "$1" | tr -d ' ')"

    [[ "$br" =~ ^\* ]] && echo "This is already the active branch." && return
    [[ -z "$br" ]] && echo "Error: couldn't find any local branch that matches \"$1\"" && return 1
    [[ $(echo "$br" | wc -l) -gt 1 ]] && echo -e "There is more than one branch matching your input:\n$br" && return 1

    git checkout "$br"
}

# Function to get the current main branch dynamically
function git-get-main-branch {
    local main_branch
    main_branch=$(basename "$(git symbolic-ref refs/remotes/origin/HEAD 2>/dev/null)")
    echo "${main_branch#refs/heads/}"
}


# Switch to main branch of a git repository, pull changes from origin, and switch back to your current working branch
function git-update-main-branch {
    if [ ! -d .git ] && ! git rev-parse --git-dir > /dev/null 2>&1; then
        echo "Error: Current directory is not a Git repository."
        return 1
    fi

    local main_branch
    local current_branch

    main_branch=$(git-get-main-branch)
    if [ -z "$main_branch" ]; then
        echo "Error: Failed to determine the main branch."
        return 1
    fi

    current_branch="$(git symbolic-ref --short HEAD 2>/dev/null)"
    if [[ "$current_branch" != "$main_branch" ]]; then
        git checkout "$main_branch" || { echo "Failed to switch to $main_branch"; return 1; }
        branch_change=true
    fi
    git pull origin "$main_branch" || { echo "Failed to pull updates from origin"; return 1; }
    [ $branch_change ] && git checkout "$current_branch"
}

# Iterates over all directories inside the current directory and updates their main branch
function git-mass-update {
    local logfile
    local errors_counter
    logfile="/tmp/${FUNCNAME[0]}.log"
    truncate -s 0 "$logfile"
    errors_counter=0

    dirs="$(ls -d ./*/)"
    for dir in $dirs; do
        echo -e "\n\n##### $(basename "$dir"):"
        cd "$dir" || { echo "Error: Fail to enter directory $dir"; return 1; }
        if ! git-update-main-branch; then
            ((errors_counter++))
            echo "Encountered errors in: $dir" >> "$logfile"
        fi
        cd ..
    done
    if [[ -n $errors_counter ]]; then
        echo "A total of $errors_counter issues were encountered. See log for detail: $logfile"
    else
        echo "${FUNCNAME[0]} finished successfully."
    fi
}

# Helper function to pause execution and prompt user to continue
function pause_and_continue {
    read -rp "Press Enter to continue after resolving the issue..."
}

## Update and rebase a given branch on top of main branch. If the branch name is not provided - do the operation over the current branch.
## This function is intended to be executed inside the top level directory of a git repository.
## Example usage: git-rebase-on-main [target_dir] [side_branch] [push] (yes/no)
##      Defaults:
##          target_dir: current directory
##          side_branch: current branch
##          push: false
##      Acceptable positive values for dry-run are 'yes' (case-insensitive) and 'true'
function git-rebase-on-main() {
    if [ "$#" -gt 3 ]; then
        echo "Usage: ${FUNCNAME[0]} [target_dir] [side_branch] [push]"
        return 1
    fi

    local push="No"
    local target_dir
    local side_branch

    if [ "$#" -ge 1 ]; then
        target_dir="$1"

        # Change directory to the path provided
        [ ! -d "$target_dir" ] && { echo "Error: Couldn't find $target_dir."; return 1; }
        cd "$target_dir" || exit;
        echo -e "\n\n\tWorking on $target_dir"

        # Validate that the repository is a Git repository
        git rev-parse --git-dir > /dev/null 2>&1 || { echo "Error: Not a Git repository."; return 1; }
    fi

    if [ "$#" -ge 2 ]; then
        side_branch="$2"
    else
        side_branch="$(git rev-parse --abbrev-ref HEAD)"
    fi

    if [ "$#" -eq 3 ]; then
        push="$3"
    fi

    # Validate the existence of the side branch
    git rev-parse --quiet --verify "$side_branch" > /dev/null 2>&1 || { echo "Error: Side branch '$side_branch' not found."; return 1; }

    # Step 1: Get the current main branch
    local main_branch
    main_branch=$(git-get-main-branch)
    if [ -z "$main_branch" ]; then
        echo "Failed to determine the main branch. Skipping."
        return
    fi

    # Step 2: Fetch updates from the remote server
    git fetch origin "$main_branch" || { echo "Failed to fetch updates from origin."; pause_and_continue; }


    # Step 3: Rebase the side branch over main
    if ! git checkout "$side_branch" || ! git rebase origin/main; then
        echo "Error: Failed to rebase the side branch over main."; pause_and_continue;
    fi

    # Update the commit timestamp
    git commit --amend --date="$(date -R)" --no-edit > /dev/null

    # Step 4: Force-push changes to the remote server (if not in dry-run mode)
    if [[ "$push" =~ ^([Yy].*|true) ]]; then
        echo "Changes will not be pushed to the remote server."
    else
        git push --force-with-lease origin "$side_branch" || { echo "Error: Failed to push changes to the remote server."; pause_and_continue; }
    fi

    # Change dir back to the previous directory
    if [ -e "$target_dir" ]; then
        cd - || return 1
    fi
}

## Mass update and rebase of a given branch on top of main branch, of multiple git repositories.
##                                <input_file>     <side_branch>        <push> <parallel_jobs>
## Example usage: git-mass-rebase "input_file.txt" "your_side_branch_name" true 4
function git-mass-rebase {
    # Validate command-line arguments
    if [ "$#" -ne 4 ]; then
        echo "Usage: ${FUNCNAME[0]} <input_file> <side_branch> <push> <parallel_jobs>"
        return 1
    fi

    local input_file="$1"
    local side_branch="$2"
    local push="$3"
    local parallel_jobs="$4"

    # Validate input file
    if [ ! -f "$input_file" ]; then
        echo "Error: Input file not found: $input_file"
        return 1
    fi

    # Log script parameters
    echo "Script parameters: input_file=$input_file, side_branch=$side_branch, push=$push, parallel_jobs=$parallel_jobs"

    # Determine the number of CPUs available for parallel execution
    local cpu_count
    cpu_count=$(nproc)

    # Determine the actual number of parallel jobs to use
    local jobs
    if [ "$parallel_jobs" -gt 0 ] && [ "$parallel_jobs" -le "$cpu_count" ]; then
        jobs="$parallel_jobs"
    else
        jobs="$cpu_count"
    fi

    # Iterate over directories in parallel
    xargs -I{} -n 1 -P "$jobs" bash -c "source $HOME/.bash_functions && git-rebase-on-main '{}' '$side_branch' '$push'" < "$input_file"

    echo "Script completed successfully"
}

## Function to clone all repositories listed in a given file
## It also uses the current directory to define if cloning a package or a container definition
## - The input file can be generated by copying the HTML table from BitBucket project view into a local file.
##    Then, execute this to only leave the repositories names:
##    `\grep -o '<a.*>.*<\/a>' index.html | sed 's/<\/\?a[^>]*>//g' > repositories.list`
function git-mass-clone {
    # Validate arguments
    if [ "$#" -lt 1 ]; then
        echo "Usage: ${FUNCNAME[0]} <input_file>"
        return 1
    fi

    local filename
    filename="$1"

    if [ ! -f "$filename" ]; then
        echo "Error: File not found - $filename."
        return 1
    fi

    project="$(basename "$(pwd)")"
    for repo in $(cat "$filename"); do
        if [ ! -d "$repo" ]; then
            git clone "ssh://git@bitbucket-cableos.harmonicinc.com:7999/$project/$repo.git"
        fi
    done
}

############################# Konsole #############################
set-konsole-tab-title-type ()
{
    local _title="$1"
    local _type=${2:-0}
    [[ -z "${_title}" ]]               && return 1
    [[ -z "${KONSOLE_DBUS_SERVICE}" ]] && return 1
    [[ -z "${KONSOLE_DBUS_SESSION}" ]] && return 1
    qdbus >/dev/null "${KONSOLE_DBUS_SERVICE}" "${KONSOLE_DBUS_SESSION}" setTabTitleFormat "${_type}" "${_title}"
}
set-konsole-tab-title ()
{
    set-konsole-tab-title-type "$1" && set-konsole-tab-title-type "$1" 1
}

############################# Yakuake #############################
function addYakuakeQuadSession {
    # Usage: addYakuakeQuadSession command-to-execute [tab-title]ESSION_ID="$(qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.addSessionQuad)"
    TERMINAL_IDS="$(qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.terminalIdsForSessionId "$SESSION_ID")"
    IFS=, IDS=("$TERMINAL_IDS")
    for terminal in "${IDS[@]}"; do
        if [[ "$1" == hvs* ]]; then
            qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.runCommandInTerminal "$terminal" "sshpass -p nsg-dev ssh $1; clear"
        else
            qdbus org.kde.yakuake /yakuake/sessions org.kde.yakuake.runCommandInTerminal "$terminal" "$1"
        fi
    done
    if [ -n "$2" ]; then
        qdbus org.kde.yakuake /yakuake/tabs org.kde.yakuake.setTabTitle "$SESSION_ID" "$2"
    elif [[ "$1" == hvs* ]]; then
        qdbus org.kde.yakuake /yakuake/tabs org.kde.yakuake.setTabTitle "$SESSION_ID" "$1"
    fi
}
