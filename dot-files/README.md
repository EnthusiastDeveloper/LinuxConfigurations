# bashrc & aliases

In this folder there are several files with reads to eachother on the user's login:

- .bash_profile which calls .bashrc file
- .bashrc sets several parameters such as PS1 and git-completion and source ```~/.bash_aliases``` and ```~/.bash_functions```

```~/.bash_aliases``` sets "ssh" command as "sshrc" - make sure to copy ```sshrc``` to your path!
This application will copy all files within ```~/.sshrc.d/``` over to the remote machine so all the configuration files are still applicable.