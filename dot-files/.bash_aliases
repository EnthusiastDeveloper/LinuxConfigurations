#
#  Global ~/.bash_aliases
#

#############  Colored Terminal  #############
alias ls='$(which ls) --color=auto --group-directories-first -hF --time-style=locale'
# Also show line numbers for grep
alias grep='grep -n --color=auto --exclude=~/.bash_history'
alias zgrep='zgrep -n --color=always'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias dir='dir --color=auto'
alias ip='ip -c'
##############################################


##############  Human Readable  ##############
alias du='du -h'
alias df='df -h'
alias free='free -h'
alias speedtest='speedtest --bytes'
##############################################


##############      Network     ##############
alias ping='ping -c 4'
alias pingmodem='ping tplink_router'
alias pingdns='ping adguard'
alias pingg='ping www.google.com'

# Execute wget with resume option
alias wget='wget -c'

# nmap shortcuts for home networks scanning
alias nmap_formatter="xmlstarlet sel -t -m '/nmaprun/host[status/@state=\"up\"]' -v 'address[@addrtype=\"ipv4\"]/@addr' -o $'\t' -v 'address[@addrtype=\"mac\"]/@addr' -o $'\t' --if 'address[@addrtype=\"mac\"]/@vendor' -v 'address[@addrtype=\"mac\"]/@vendor' -o $'\t' -b --if 'hostnames/hostname[1]' -v 'hostnames/hostname[1]/@name' -b -n"
alias nma='sudo nmap -sn 192.168.1.0/24 -oX - | nmap_formatter'
alias nma8='sudo nmap -sn 192.168.8.0/24 -oX - | nmap_formatter'
alias nmap='sudo nmap'

# Wake-On-Lan
alias wolomv='wol 24:5e:be:19:d4:9f'

alias ssh=sshrc
##############################################


##############    Auto  sudo    ##############
# When using sudo, use alias expansion (otherwise sudo ignores aliases)
alias sudo='sudo '
alias watch='watch '
alias notify='notify '
alias reboot='sudo reboot'
alias shutdown='sudo shutdown now'
alias find='sudo find'
##############################################


#############  Package Managers  #############
alias aurman='LC_ALL=C aurman'
alias pacman='sudo pacman'
alias pacmirrorsupdate='sudo /usr/bin/reflector --protocol https --country Israel --country Germany --age 12 --latest 30 --number 20 --sort rate --save /etc/pacman.d/mirrorlist'
alias pacmirrorsupdate2='curl -s "https://www.archlinux.org/mirrorlist/?country=DE&country=IL&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > ~/mirrorlist'
alias pacremorphan='sudo pacman -Rns $(pacman -Qdtq)'
alias pacdiff='sudo pacdiff'

alias apt='sudo apt'
alias apts='apt-cache search'
alias aptshow='apt-cache show'
alias aptinst='sudo apt-get install -V'
alias aptup='sudo apt update; clear; apt list --upgradable; read -p "Upgrade now? [y/n]  " input; [ ${input^^} == "Y" ] && sudo apt-get -y upgrade || echo "OK, bye."'
alias aptupg='sudo apt-get dist-upgrade -V && sudo apt-get autoremove'
alias aptrm='sudo apt-get remove'
alias aptpurge='sudo apt-get remove --purge'
##############################################


######   Interactive File Interaction   ######
alias mv='mv -i'
alias cp='cp -i'
alias rm='rm -i'
##############################################


############    Network Shares   #############
alias omv-cifs='smbclient -L OMV'
alias omv-samba='smbclient -L OMV'
alias mnt-multimedia='sudo mount -t cifs //omv/OMV-Multimedia /mnt/OMV/Multimedia/ -o credentials=$HOME/.nas_credentials'
alias mnt-plex-vid='sudo mount -t cifs //omv/Plex_Videos /mnt/OMV/plex_vid/ -o credentials=$HOME/.nas_credentials'

##############################################


##################    Git   ##################
alias gits='git status'
##############################################


##########    Debian Build system   ##########
alias dch='dch --upstream -Dunstable --nmu --urgency=low'
SCHROOT_PI="pi35"
alias build='nsg-schroot-run $SCHROOT_PI-apollo -- dpkg-buildpackage -us -uc -b -j4'
alias buildd='nsg-schroot build-deps $SCHROOT_PI-apollo . && build'
alias nsg-stop-all-sessions='for s in `nsg-schroot list-all | awk '\''{if($1 == "session::") {print $2}}'\''`; do nsg-schroot session-end "$s"; done'
##############################################


##########       Navigations       ###########
alias ..='cd ..'
alias ...='cd ../..'
alias cd..='cd ..'
alias cd...='cd ../..'
##############################################


##############  Various tweaks  ##############
alias l='ls -A'
alias ll='ls -lA'
alias llt='ll -rt -I .directory'

alias c='clear'
alias cdc='cd; clear'

alias wl='wc -l'

alias reload='source ~/.bashrc'
alias brc='$EDITOR ~/.bashrc && reload'
alias bal='$EDITOR ~/.bash_aliases && reload'
alias bfn='$EDITOR ~/.bash_functions && reload'
alias sshc='$EDITOR ~/.ssh/config'

alias xclip='xclip -selection c'

alias j=jobs
alias h=history

# Create parent directories if needed
alias mkdir='mkdir -pv'

# Enable the following flags for nano: 
#	-A -> Enable smart home key
#   -F -> Enable multibuffer
#   -S -> Enable smooth scrolling
#	-c -> Constantly show cursor position
#	-i -> Automatically indent new lines
#	-l -> Show line numbers in front of the text
#   -m -> Enable mouse support in X window env
#	-u -> Save a file by default in Unix format
#	-z -> Enable suspension
#	-s -> Enable alternate speller
alias nano='nano -AFScilmuz -s "aspell -c -x" --tabsize=4'

alias today='date +%A, %B %-d %Y'

##############################################

# Disk Space usage
alias usage='df -hlT --exclude-type=tmpfs --exclude-type=devtmpfs'
alias most='du -hsx * | sort -rh | head -10'
alias big_files='find "$dir" -type f -size +20M -exec ls -lh {} \; 2> /dev/null | awk { print \t( ,, )\t } | sort -hrk 1'

# Auto clear vivaldi corupted datafiles
alias vivaldi-empty-cache='\rm -r "$(du -hsx $HOME/.config/vivaldi/Default/Service\ Worker/* | sort -rh | head -n1 | cut -f2)"'
#du -hsx $HOME/.config/vivaldi/Default/Service\ Worker/* | sort -rh | head -n1 | cut -f2 | rm -r'

# Joplin update
alias joplinup='wget -O - https://raw.githubusercontent.com/laurent22/joplin/dev/Joplin_install_and_update.sh | bash'

