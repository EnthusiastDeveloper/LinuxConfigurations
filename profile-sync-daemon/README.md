# Profile Sync Daemon

PSD is a tiny pseudo-daemon designed to manage browser profile(s) in tmpfs and to periodically sync back to the physical disc.  

__Pros:__
* Faster browsing experience
* Less write operations to disk
* When using overlayFS sync and unsync operations are performed faster

__Cons:__
* Might slow down login process due to copying the browser's profile to RAM
* When using overlayFS, it takes more disk space (as a read only partition) in order to minimize the RAM usage


To start, install [profile-sync-daemon](https://aur.archlinux.org/packages/Profile-sync-daemon/) from the AUR.  

Then, follow the instructions on the [Arch Wiki page](https://wiki.archlinux.org/index.php/profile-sync-daemon) for configurations guide.


The configuration file will be in ```/home/<user>/.config/psd/psd.conf```
