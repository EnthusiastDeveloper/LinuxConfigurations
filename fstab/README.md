# Laptop's fstab:

### /tmp folder  
Default Arch Linux sets /tmp as a tmpfs directory, which means it resides in the machines' RAM.
By adding /tmp parameters to fstab I'm able to set its size to 2GB, as opposed to half of the system's RAM.

Regarding the 'discard' flag (which activates the SSD TRIM functionality) - its use is deprecated and the recommended method is to use
``` shell
systemctl enable fstrim.timer
```
The util-linux package provides fstrim.service and fstrim.timer systemd unit files. Enabling the timer will activate the service weekly.
The service executes [fstrim(8)](https://jlk.fjfi.cvut.cz/arch/manpages/man/fstrim.8) on all mounted filesystems on devices that support the discard operation.

### Mounting network folders (samba)  
In order to mount samba shared folders install: ```smbclient``` and create an empty ```/etc/samba/smb.conf``` file (unless you also run a local samba server as well...). The example ```smb.conf``` file is attached.  

* For manual mounting :  
    ```shell
    mount -t cifs //server/folder /mnt/point -o credentials=/path/to/credentials.file,workgroup=WORKGROUP,iocharset=utf8,vers=3,uid=1001,gid=1000
    ```  
    <b>where:</b>
    <ul>
        <li><em>//server</em> - is as defined in /etc/hosts file</li>
        <li>
        <em>credentials</em> - is a text file which holds the server login credentials.  
            It helps avoiding writing the password in a readable text.  
            This file is owned by root and <code>chmod</code> it to 600!
        </li>
        <li>
        <em>vers</em> - is the samba protocol version
        </li>
        <li>
        <em>uid & gid</em> - of remote user
        </li>
    </ul>

* Systemd mounting :
    1. Create a .mount unit file at <code>/usr/lib/systemd/system/</code>. File name should match the Where= value. Failing to meet this criteria might result an error:  
        <i>systemd[1]: mnt-myshare.mount: Where= setting does not match unit name. Refusing.</i>
        So, if mount point is on /mnt/folder, the filename should be mnt-folder.mount
    2. To avoid mount errors at boot time when using the server's hostname instead of its IP, add <code>systemd-resolved.service</code> to <code>After</code> and <code>Wants</code>.
    3. Run ```systemctl daemon-reload``` to make systemd aware of the new file.
    4. Run ```systemctl start/enable unit.mount``` to mount the folder.



# Proxmox

Special configurations for proxmox machine to mount its dedicated backups folder on NAS server as root, to only be used temporarily for verifying\downloading files.

Both the below methods requires:
* <code>/etc/hosts</code> configuration of `NAS`
* Local user on NAS server with:
    - R+W permissions for the target backups folder
    - Application privilege for `Microsoft Networkoing` (Samba sharing)
* Credentials file in `/root/.nas_backup_credentials` with:
    - The above NAS user & its password
    - NAS domain name (default is `WORKGROUP`)
    - Preferably the file should only be accessible by the root account


### Mounting methods:
1. Systemd mounting :
    Copy <code>mnt-backups.mount</code> to <code>/usr/lib/systemd/system/</code> and run <code>systemctl enable/start</code> it.
2. On-demand mounting:
    ``` shell
    mount -t cifs //NAS/Proxmox /mnt/backups -o iocharset=utf8,rw,credentials=/root/.nas_backup_credentials,uid=0,gid=0
    ```
    