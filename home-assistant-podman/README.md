# Home Assistant podman stack

These scripts will interactively create the basic configuration files for Home assistant deployment over podman containers, as well as SystemD service files for automatically run those containers.
It will also create a new user under which all the above containers will be executed and will also install podman if needed.

### To use these scripts:
1. Copy ```server_init.sh```, ```config.sh```, and ```data.tar.gz``` to your desired location.
2. Rename ```config_template.sh``` to ```config.sh``` and replace stubs with your actual data.
3. Execute the ```server_init.sh``` and follow the on-screen instructions.
