##
## This is the configuration file for the Home Assistant podman-deployment script
##

# Working directory for all data and configurations of the home assistant containers stack
export PODMAN_WORKDIR='/opt/podman/volumes/hass_stack'

# User that will run the home assistant containers stack
export USERNAME='target user-name'

# MQTT broker users passwords
export MQTT_HA_USER_PASS="secret-password"
export MQTT_ZIGBEE_USER_PASS="secret-password-2"

# The IP adresses space of your network (e.g. 192.xxx.xxx.xxx). Used to find the machine's LAN IP, required for MQTT config file and for adding ipanels.
export IP_SPACE="192"

# USB Zigbee dongle. You can find its value using `ls -l /dev/serial/by-id/`
export ZIGBEE_DONGLE="/dev/serial/by-id/usb-ITead_Sonoff_Zigbee_3.0_USB_Dongle_Plus_...-port0"
