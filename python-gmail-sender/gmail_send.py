"""
Simple script for sending an email through GMail.
Requires to add the password in plain text or use specific App-password
"""

import smtplib
from email.mime.text import MIMEText

SENDER = "sender@gmail.com"
PASSWORD = 'app-password goes here'
RECIPIENTS = ['recipient_1@gmail.com', 'recipient_2@gmail.com']


def send_email(subject: str, body: str, recipients: list[str] = RECIPIENTS) -> None:
    """
    Constructs am email message with given arguments and sends it via SMTP-SSL
    :param subject:
    :param body:
    :param recipients: list of strings, each one representing an email
    """
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['To'] = ', '.join(recipients)
    msg['From'] = SENDER
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp_server:
        smtp_server.login(SENDER, PASSWORD)
        smtp_server.sendmail(SENDER, recipients, msg.as_string())


# Running this script will execute this line.
# Use it to verify the above configurations 
send_email("Python script test", "This is a test email from python script")
