# Python Gmail sender:

## Before you start:
Make sure to create a new app-password to be used with this script (requires 2-step verification to be enabled).
Go to your [Google Account](https://myaccount.google.com/) -> Security -> 2-Step Verification. At the bottom of the page select "App passwords" and create a new one.

## Configurations:
Set your source email address and it's app-password as values for `SENDER` and `PASSWORD` respectively
Add a list of recipients into the `RECIPIENTS` variable (one is totally exceptable!)

## Usage:
As simple as:

```python
from gmail_send import send_email

send_email("Subject", "body")
```
