# GRUB Defaults

This file is used by grub-mkconfig in order to generate the grub.cfg.  
Its location is ```/etc/default/grub```

Changes from the default GRUB configuration file are in:  
<strong>Line #7</strong>: Setting the kernel scheduler to Multi-Queue, which maps I/O queries to multiple queues, the tasks are distributed across threads and therefore CPU cores.  
<strong>Line #28</strong>: Adds 1024x768 as the first screen resolution, which makes GRUB menu easier to read on 13.3" laptop screen  
<strong>Line #46</strong>: Adds an image as the backgroung image for GRUB menu. Purely cosmetics.  
