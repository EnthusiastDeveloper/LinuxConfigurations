# Pacman:

## pacman.conf

Lives in ```/etc```.  
Defines all pacman configurations like repositories, architecture, hooks folder etc.


## Hooks

To define your own hooks (like ```arch-audit.hook```):
1. Uncomment the `HookDir` row in pacman.conf
2. Create the hooks directory (like ```/etc/pacman.d/hooks/```)
3. Place them in the folder.

Note: hook files must have `.hook` suffix.


### pacman-cleanup-hook:
Just install [pacman-cleanup-hook](https://aur.archlinux.org/packages/pacman-cleanup-hook/), no configurations required.


### arch-audit.hook:
Post-transaction hook to list vulnerable packages based on Arch CVE.
Depends on packages:
- `curl`
- `openssl`
- `arch-audit`


### update-mirrorlist.hook:
Post-transaction hook to update pacman's mirrorlist and remove mirrorlist.pacnew.  
Depends on `reflector`.
