# WhatsApp Standalone App

Backup for the WhatsApp standalone web app.  
The hidden png file is used as the icon for it.  
For the icon to work properly, copy it to your desired location and update WhatsApp.desktop on line #7:  
``` bash
Icon=/path/to/image/.whatsapp.png
```

In order to add this file to KDE menu - use ```kmenuedit``` and add it as a new item to the relevant subfolder.



## Generating a new app-like web page

1. Open the desired web page in Chromium.
2. Open the top-right menu -> More Tools -> Add to Desktop / Create Shortcut
3. Type a name for it and make sure to select ```Open as window```
4. A new .desktop file will be created in ~/Desktop


### IMPORTANT!  
<strong>You must have Chromium package installed in order for it to work.</strong>
