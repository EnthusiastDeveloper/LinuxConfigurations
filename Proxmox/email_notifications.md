# Proxmox email notifications

This guide is here to configure email notifications from Proxmox VE.
It has been verified working over proxmox VE 6.0 

## Guide

1. Install the authentication library: `apt-get install libsasl2-modules`
3. If Gmail has 2FA enabled, go to `App Passwords` and generate a new password just for Proxmox
4. Create a password file: `nano /etc/postfix/sasl_passwd`
5. Insert your login details: `smtp.gmail.com youremail@gmail.com:yourpassword`
6. Save the password file
7. Create a database from the password file: `postmap hash:/etc/postfix/sasl_passwd`
8. Protect the text password file: `chmod 600 /etc/postfix/sasl_passwd`
9. Edit the postfix configuration file: `nano /etc/postfix/main.cf`
10. Add/change the following (certificates can be found in /etc/ssl/certs/):
<code>
    relayhost = smtp.gmail.com:587
    smtp_use_tls = yes
    smtp_sasl_auth_enable = yes
    smtp_sasl_security_options =
    smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
    smtp_tls_CAfile = /etc/ssl/certs/Entrust_Root_Certification_Authority.pem
    smtp_tls_session_cache_database = btree:/var/lib/postfix/smtp_tls_session_cache
    smtp_tls_session_cache_timeout = 3600s
</code>
11. Reload the updated configuration: `postfix reload`

## Testing
``` shell
echo "test message" | mail -s "test subject" youremail@gmail.com
```
